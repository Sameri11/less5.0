variable "web_instance_count" {
  type    = number
  default = 3
}